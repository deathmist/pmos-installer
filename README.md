# pmOS-installer

A relatively nice postmarketOS installer for distributing pmOS to end users.

**NOTE:** The installer doesn't currently support QCDT devices

Run `./makeinstaller.sh -h` for options, it is possible to create a minimal flashable zip (compatible with TWRP) that also allows users to select which boot slot to flash to.

The installer needs to know which partition is the userdata partition (where to install to), you can find this by running `ls -la /dev/block/bootdevice/by-name/userdata` from TWRP or Android and looking at the symlink, e.g.

```sh
OnePlus6:/ # ls -la /dev/block/bootdevice/by-name/userdata
lrwxrwxrwx 1 root root 16 1970-03-20 14:18 /dev/block/bootdevice/by-name/userdata -> /dev/block/sda17
```

## Running the script

Before generating an installer, you have to build postmarketos. A split install is required as we only want the rootfs, not the extra boot partition.
```
pmbootstrap install --split
# Export to make it available to the script
pmbootstrap export
```

Now generate the installer with
```
# Replace 'PARTITION' with the partition you got earlier, e.g. 'sda17'
./makeinstaller.sh -p PARTITION
```

> **NOTE:** You can alternatively specify a ramdisk to use, this will make the partition optional

## Parameters

### SLOT_x (*optional*)

Selects which slot the boot image should be flashed to. Valid values are:

* `a` to flash to the A slot (`/dev/block/bootdevice/by-name/boot_a`).
* `b` to flash to the B slot (`/dev/block/bootdevice/by-name/boot_b`).
* Any other value to flash to the current slot (`/dev/block/bootdevice/by-name/boot`).

The values are case sensitive (and must be lower case).

If your device does not use slots, do not include this parameter