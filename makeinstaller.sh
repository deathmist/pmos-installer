#!/bin/bash

THIS_SCRIPT=$0

log() {
	echo "^ $1"
}

# $1: The var to replace
# $2: The new value
# $3: The file to run on
# e.g. set_template_var UIFLAVOUR Phosh /etc/hostname
set_template_var() {
	sed "s/%$1%/$2/g" -i $3
}

die() {
	[[ ! -z $1 ]] && echo "$1"
	exit 1
}

mkinitfs () {
	OUT=$(realpath $2)
	echo "Creating initramfs: $OUT"
	pushd $1 > /dev/null
	find . -print0 | cpio --null --create --format=newc | gzip --best > $OUT
	popd > /dev/null
}

BASEDIR="$(dirname $0)"
WORKDIR="./out"
PMBWORK="$(pmbootstrap config work)"
PMBDEVICE="$(pmbootstrap config device)"
KERNEL=$(echo $PMBWORK/chroot_rootfs_$PMBDEVICE/boot/vmlinuz-*-dtb)
RAMDISK_DEFAULT="$BASEDIR/initramfs.cpio.gz"

usage() {
	echo "$THIS_SCRIPT: A postmarketOS flashable installer generator"
	echo "Run 'pmbootstrap install --split && pmbootstrap export' to build pmOS"
	echo "Then run '$THIS_SCRIPT -p PARTITION' where PARTITION is the userdata partition (e.g. 'sda17')"
	echo "to generate a flashable zip."
	echo "$THIS_SCRIPT -p PARTITION [ -r RAMDISK] [ -k KERNEL ] [ -i ROOTFS_IMAGE ] [ -s SLOT ] [ -a ] " 1>&2
	exit 1
}

while getopts "p:r:k:i:s:ah" options; do
	case "${options}" in
		p)
			PART=${OPTARG}
			;;
		r)
			RAMDISK=${OPTARG}
			;;
		k)
			KERNEL=${OPTARG}
			;;
		i)
			PMOS_ROOTFS=${OPTARG}
			;;
		s)
			SLOT=${OPTARG}
			;;
		a)
			A_ONLY=1
			;;
		h)
			usage
			;;
		:)
			die "-${OPTARG} requires an argument"
			;;
		*)
			usage
			;;
	esac
done

if [ -z $PART ] && [ -z $RAMDISK ]; then
	die "-p option is missing, please see https://gitlab.com/sdm845-mainline/pmtools#pmos-installer for details"
fi

if [ ! -e /tmp/postmarketOS-export/$PMBDEVICE-root.img ]; then
	die "Please run 'pmbootstrap install --split && pmbootstrap export' before this script"
fi

hash cpio || die "Can't find cpio binary, is cpio installed?"
hash gzip || die "Can't find gzip binary, is gzip installed?"

log "Populating variables from pmbootstrap..."
log "NOTE: You must specify a kernel Image if you have built for multiple devices without running 'pmbootstrap zap'"

DEVICE=$(echo $PMBDEVICE | cut -d"-" -f2)
UIFLAVOUR=$(pmbootstrap config ui)
SRCDIR="pmos-installer"
SCRIPT="$WORKDIR/META-INF/com/google/android/update-binary"
INIT="$BASEDIR/initramfs/init"

# Grab chroot os-release variables
VERSION=$(cat $PMBWORK/chroot_rootfs_$PMBDEVICE/etc/os-release | grep "VERSION_ID=" | cut -d"=" -f2 | cut -d"\"" -f2)

log "PostmarketOS Version is: '$VERSION'"
log "UI flavour is: '$UIFLAVOUR'"
log "Working directory is: '$WORKDIR'"
log "Device is: '$DEVICE'"
log "Using kernel: '$KERNEL'"
[[ ! -z $RAMDISK ]] && log "Using ramdisk: '$RAMDISK'"
echo "--------------------------------------"

# Clean work dir
[[ ! -z $WORKDIR ]] && [[ -f $WORKDIR ]] && rm -r "$WORKDIR"

mkdir -p "$WORKDIR"

cp -r $SRCDIR/* $WORKDIR/

if [ -z $RAMDISK ]; then
	# Need to root to copy the special files (dev/console) in the initramfs
	# So to keep our template we need some hackery...
	cp ./init-template $INIT
	set_template_var "PART" "$PART" "$INIT"
	mkinitfs $BASEDIR/initramfs $RAMDISK_DEFAULT
	rm $INIT
	RAMDISK=$RAMDISK_DEFAULT
fi

set_template_var "VERSION" "$VERSION" "$SCRIPT"

set_template_var "UIFLAVOUR" "$UIFLAVOUR" "$SCRIPT"

set_template_var "DEVICE" "$DEVICE" "$SCRIPT"

# User options >>>
ROOTFS_SIZE=16
# echo
# while ! [[ "$ROOTFS_SIZE" =~ ^[0-9]+$ ]]; do
#     read -p "Enter a size in gigabytes for the final rootfs (it will be expanded during installation) [8]: " ROOTFS_SIZE
#     [[ -z $ROOTFS_SIZE ]] && ROOTFS_SIZE=8
# done

# Only enters loop if SLOT is not set by cmdline (and not building for an A-only Treble device)
if [ -z "$SLOT$A_ONLY" ]; then
	SLOT="a"
	log "Will install to slot $SLOT, modify the zip file name to change target slot"
	log "e.g. \"postmarketOS-20200806-enchilada-phosh-SLOT_a.zip\""
fi

set_template_var "SIZE" "$ROOTFS_SIZE" "$SCRIPT"
# << User options

[[ -z $PMOS_ROOTFS ]] && PMOS_ROOTFS="$(readlink /tmp/postmarketOS-export/$PMBDEVICE-root.img)"
[[ -z $PMOS_ROOTFS ]] && echo "Can't find rootfs image, make sure you run pmbootstrap install with --split"

log "Copying exported rootfs to zip directory"
if [ $(which rsync 2> /dev/null) ]; then
	rsync $PMOS_ROOTFS $WORKDIR/pmos_root.img
else
	cp $PMOS_ROOTFS $WORKDIR/pmos_root.img
fi

log "Using kernel image: $KERNEL"
log "Using ramdisk: $RAMDISK"

# Source the deviceinfo file so we can get the offsets for mkbootimg
. "$PMBWORK/cache_git/pmaports/device/testing/device-$PMBDEVICE/deviceinfo"

CMD="mkbootimg \
--base $deviceinfo_flash_offset_base \
--kernel_offset $deviceinfo_flash_offset_kernel \
--ramdisk_offset $deviceinfo_flash_offset_ramdisk \
--second_offset $deviceinfo_flash_offset_second \
--tags_offset $deviceinfo_flash_offset_tags \
--pagesize $deviceinfo_flash_pagesize \
--kernel $KERNEL \
--ramdisk $RAMDISK \
-o $WORKDIR/pmos_boot.img"

echo "$ $CMD"
$CMD

if [ -z $A_ONLY ]; then
	OUTFILE="$PWD/postmarketOS-$(date +"%Y%m%d")-$DEVICE-$UIFLAVOUR-SLOT_$SLOT.zip"
else
	OUTFILE="$PWD/postmarketOS-$(date +"%Y%m%d")-$DEVICE-$UIFLAVOUR.zip"
fi

log "Creating flashable zip: $OUTFILE"
echo

pushd $WORKDIR > /dev/null
	time zip -r "$OUTFILE" . || die "Couldn't create flashable zip!"
popd > /dev/null

rm -r $WORKDIR

echo
log "Flashable zip at $OUTFILE"
echo 
echo -e "\e[32m===================\e[0m"
echo -e "\e[32m    ALL DONE!\e[0m"
echo -e "\e[32m===================\e[0m"
